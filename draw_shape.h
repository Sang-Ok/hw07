#ifndef DH
#define DH
#include <vector>

using namespace std;

class Canvas {
public:
 Canvas(size_t row, size_t col);
 ~Canvas();
 void Resize(size_t w, size_t h){w_=w,h_=h;}

  void Draw(vector<string> s, int x, int y, char c);

private:
 int w_,h_;
 vector<Shape> shapes_;
 friend ostream& operator<<(ostream& os, const Canvas& c);
};

class Shape {
public:
  virtual void Draw(vector<vector<char> >& o) const = 0;
  int x_,y_;
  char brush_;
};

class Rectangle : public Shape { 
public:
  virtual void Draw(vector<vector<char> >& o) const;
  int w_,h_;

}

class UpTriangle : public Shape {
public:
  virtual void Draw(vector<vector<char> >& o) const;
  int size_;
}

class DownTriangle : public Shape {
public:
  virtual void Draw(vector<vector<char> >& o) const;
  int size_;
}


istream& operator>>(istream& is, Rectangle& r);
istream& operator>>(istream& is, UpTriangle& t);
istream& operator>>(istream& is, DownTriangle& d);

#endif
