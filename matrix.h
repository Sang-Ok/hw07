//matirix.h
#ifndef _MATRIX_H_
#define _MATRIX_H_
#include <iostream>
#include <vector>
using namespace std;
#define ccout cout<<"\t\tTEST::"

class Matrix {
public:
  Matrix(const Matrix& m):rows_(m.rows_), cols_(m.cols_),values_(m.values_){}
  Matrix(int rows=1, int cols=1):rows_(rows), cols_(cols){values_.resize(rows*cols);}
  int rows() const { return rows_; }
  int cols() const { return cols_; }
  void Resize(int rows, int cols) {    values_.resize((rows_ = rows) * (cols_ = cols));  }

  int& operator()(int r, int c){return values_[Sub2Ind(r,c)];}
  const int& operator()(int r, int c) const{return values_[Sub2Ind(r,c)];}
  Matrix operator+() const;
  Matrix operator-() const;
  Matrix Transpose() const;

private:
  int Sub2Ind(int r, int c) const { return r + c * rows_; }
  std::vector<int> values_;
  int rows_, cols_;
};

class Vector : public Matrix {
public:
  Vector(int rows=1) : Matrix(rows,1){}
  Vector(const Vector& v) : Matrix(v){}

  int rows() const{return Matrix::rows();}
  int cols() const{return Matrix::cols();}
  void Resize(int rows){Matrix::Resize(rows,1);}
  int& operator[](int r){Matrix::operator()(r,1);}
  int& operator()(int r) {Matrix::operator()(r,1);}
  const int& operator()(int r) const{Matrix::operator()(r,1);}
};


istream& operator>>(istream& is, Matrix& m);
istream& operator>>(istream& is, Vector& v);
ostream& operator<<(ostream& os, const Matrix& m);

Matrix operator+(const Matrix& lm, const Matrix& rm);
Matrix operator-(const Matrix& lm, const Matrix& rm);


Matrix operator*(const Matrix& lm, const Matrix& rm);

Matrix operator+(const int& a, const Matrix& rm);
Matrix operator-(const int& a, const Matrix& rm);
Matrix operator*(const int& a, const Matrix& rm);
Matrix operator+(const Matrix& lm, const int& a);
Matrix operator-(const Matrix& lm, const int& a);
Matrix operator*(const Matrix& lm, const int& a);

#endif
