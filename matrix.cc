//matirx.cc
#include "matrix.h"
#include <cstdlib>
// 단항연산자 (Unary operators)
Matrix Matrix::operator+() const{return *this;}
Matrix Matrix::operator-() const{
  Matrix mat(cols(),rows());
  for(int r=0; r<rows(); r++)
    for(int c=0; c<cols(); c++)
      mat(c,r)=(*this)(r,c);
  return mat;
}
Matrix Matrix::Transpose() const{
  Matrix mat(rows(),cols());
  for(int r=0; r<rows(); r++)
    for(int c=0; c<cols(); c++)
      mat(r,c)=-(*this)(r,c);
  return mat;
}

ostream& operator<<(ostream& os, const Matrix& m){
  for(int r=0; r<m.rows(); r++){
    for(int c=0; c<m.cols(); c++)
      os<<m(r,c)<<' ';
    os<<endl;
  }
  return os;
}

istream& operator>>(istream& is, Matrix& m){
  int r,c;
  is>>r>>c;
  m.Resize(r,c);
  for(r=0; r<m.rows(); r++)
    for(c=0; c<m.cols(); c++)
      is>>m(r,c);
  return is;    
}
istream& operator>>(istream& is, Vector& m){
  int r,c=1;
  is>>r;
  m.Resize(r);
  for(r=0; r<m.rows(); r++)
    is>>m(r);
  return is;
}

Matrix operator+(const Matrix& lm, const Matrix& rm){
  if(lm.rows()!=rm.rows() || lm.cols()!=rm.cols()){
    cout << "Invalid operation" << std::endl;
    exit(0);
  }
  Matrix ret(lm.rows(),lm.cols());
  for(int r=0; r<lm.rows(); r++)
    for(int c=0; c<lm.cols(); c++)
      ret(r,c)=lm(r,c)+rm(r,c);
  return ret;
}
Matrix operator-(const Matrix& lm, const Matrix& rm){
  if(lm.rows()!=rm.rows() || lm.cols()!=rm.cols()){
    cout << "Invalid operation" << std::endl;
    exit(0);
  }
  Matrix ret(lm.rows(),lm.cols());
  for(int r=0; r<lm.rows(); r++)
    for(int c=0; c<lm.cols(); c++)
      ret(r,c)=lm(r,c)-rm(r,c);
  return ret;
}

Matrix operator*(const Matrix& lm, const Matrix& rm){
  if(lm.cols()!=rm.rows()){
    cout << "Invalid operation" << std::endl;
    exit(0);
  }
  Matrix ret(lm.rows(),rm.cols());
  for(int r=0; r<lm.rows(); r++)
    for(int c=0; c<rm.cols(); c++){
      int s=0;
      for(int k=0; k<lm.cols(); k++)
        s+=lm(r,k)*rm(k,c);
      ret(r,c)=s;
    }
  return ret;
}

Matrix operator+(const int& a, const Matrix& rm){
  Matrix ret(rm.rows(),rm.cols());
  for(int r=0; r<rm.rows(); r++)
    for(int c=0; c<rm.cols(); c++)
      ret(r,c)=a+rm(r,c);
  return ret;
}
Matrix operator-(const int& a, const Matrix& rm){
  Matrix ret(rm.rows(),rm.cols());
  for(int r=0; r<rm.rows(); r++)
    for(int c=0; c<rm.cols(); c++)
      ret(r,c)=a-rm(r,c);
  return ret;
}
Matrix operator*(const int& a, const Matrix& rm){
  Matrix ret(rm.rows(),rm.cols());
  for(int r=0; r<rm.rows(); r++)
    for(int c=0; c<rm.cols(); c++)
      ret(r,c)=a*rm(r,c);
  return ret;
}
Matrix operator+(const Matrix& lm, const int& a){return a + lm;}
Matrix operator-(const Matrix& lm, const int& a){return -a+ lm;}
Matrix operator*(const Matrix& lm, const int& a){return a * lm;}

